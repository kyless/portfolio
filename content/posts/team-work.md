---
title: Team Work
date: 2020-01-09
published: true
cover_image: null
tags: ['Team','Thoughts']
canonical_url: false
description: "Some thoughts about how to better work on teams."
---

Hey everyone! This is my first blog post. I've been a software developer for 6 months now at a large corporation. I work on a team that has had a lot of turnover in the past. As I've been learning how to improve the team, I had some thoughts I wanted to share. 

# Eliminate Knowledge Silos

Knowledge silos happen when you (the developer) have written a piece of code, stood up a random server, or solely worked on something without the help or knowledge of your team. These tend to happen on teams that do not operate in a truly agile method. **No** knowledge silos do not help your job security. They generally hurt your team when you leave the company because information was not shared. Make sure that you work in such a way that encourages knowledge sharing and don’t be afraid to work in pairs on things. Care more about the team’s success than your own job security.

# There is only one priority

Teams often get the mindset that they can work on multiple things at once. This comes from an honest belief that with numbers you can accomplish more work. However, this helps encourage knowledge silos. Instead, work on things together as a team where everyone is aligned on the purpose and mission. Make sure that you have fully attacked the top priority with as many resources as possible before starting new work. If the team is not able to identify the top priority, then they will fail to work as efficiently as possible. 

# Maybe it’s not about getting credit

Gene Kim recently wrote a book called [The Unicorn Project](https://itrevolution.com/the-unicorn-project/). He profoundly stated: 
> You can get a lot more done if you don’t care that much about who gets the credit. 

Work on developing the team and thinking from a team mindset more. We all tend to get selfish in our work and end up responding and working in ways that make us look more intelligent than those arounds us. Work to care more about the team’s reputation than your own.